<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>testsuite2</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>6f819558-f85d-4fc9-9807-45ed95f80c46</testSuiteGuid>
   <testCaseLink>
      <guid>5622554d-3e9e-4103-a42c-7deec9a8781a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/testcase2.1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f9a39908-8836-41e9-b280-0f4ac0bdaddc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/testcase2.2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0b44eaa6-3f87-4ca9-a4ac-50550827437b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/testcase2.3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d86c3444-899e-461d-b6b7-d2ab68fde2f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/testcase2.4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>54948c88-7cdb-4322-9944-e9d54c67150f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/testcase2.5</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
