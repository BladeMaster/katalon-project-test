import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://52.70.201.210:9999/')

WebUI.setText(findTestObject('Page_ProjectBackend/input_Username_admin'), 'admin')

WebUI.setEncryptedText(findTestObject('Page_ProjectBackend/input_Password_admin'), 'RAIVpflpDOg=')

WebUI.click(findTestObject('Page_ProjectBackend/button_Login'))

WebUI.click(findTestObject('Page_ProjectBackend/a_Total Transaction'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/td_Garden Papaya'), 'Garden, Papaya')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/td_Banana Garden Banana Rambutan'), 'Banana, Garden, Banana, Rambutan')

WebUI.verifyElementText(findTestObject('Object Repository/Page_ProjectBackend/p_Total price  80690 THB'), 'Total price: 80,690 THB')

WebUI.closeBrowser()

